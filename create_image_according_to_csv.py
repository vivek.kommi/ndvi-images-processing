from PIL import Image
from utils import should_consider_image, count_ndvi_from_rgb
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def map_image_according_to_csv(img_file_path, csv_file_path, out_file_name, dpi=600):
    # Open image
    img = Image.open(img_file_path)
    img_w, img_h = img.size

    # Split image into channels
    img_r, img_g, img_b, img_a = img.split()

    # Covert channels to arrays
    img_arr_r = np.asarray(img_r).astype('float32')
    img_arr_g = np.asarray(img_g).astype('float32')
    img_arr_b = np.asarray(img_b).astype('float32')

    # Read pixel mappings
    pixel_mapping = {}

    data = pd.read_csv(csv_file_path, sep=';')

    for x in range(0, len(data.values)):
        pixel_mapping[data.values[x][0]] = data.values[x][1]

    # Map pixels
    ndvi_data = np.zeros((len(img_arr_r), len(img_arr_r[0])), dtype=np.float)

    for x in range(0, len(img_arr_r)):
        for y in range(0, len(img_arr_r[x])):
            if should_consider_image(img_arr_r[x][y], img_arr_g[x][y], img_arr_b[x][y]):
                pixel_density = int(img_arr_r[x][y])
                mapped_pixel_density = pixel_mapping[pixel_density]
                ndvi_data[x][y] = count_ndvi_from_rgb(mapped_pixel_density, mapped_pixel_density, mapped_pixel_density)
            else:
                ndvi_data[x][y] = -1

    fig_w = img_w / dpi
    fig_h = img_h / dpi
    fig = plt.figure(figsize=(fig_w, fig_h), dpi=dpi)
    fig.set_frameon(False)

    # Make an axis for the image filling the whole figure except colorbar space
    ax_rect = [
        0.0,  # left
        0.0,  # bottom
        1.0,  # width
        1.0]  # height

    ax = fig.add_axes(ax_rect)
    ax.yaxis.set_ticklabels([])
    ax.xaxis.set_ticklabels([])
    ax.set_axis_off()
    ax.patch.set_alpha(0.0)

    # Create plot
    custom_cmap = plt.set_cmap('jet')
    axes_img = ax.imshow(ndvi_data,
                         cmap=custom_cmap,
                         vmin=min,
                         vmax=max,
                         aspect='equal'
                         )

    # Write to file
    cax = fig.add_axes([0.95, 0.05, 0.025, 0.90])
    cbar = fig.colorbar(axes_img, cax=cax)
    fig.savefig(out_file_name, dpi=dpi)
    plt.close(fig)


if __name__ == '__main__':
    map_image_according_to_csv('input/screenshot-2020.png', 'input/mapping_csv.csv', 'output/2020-mapped-ndvi.png')
    print('Finished 2020')
    map_image_according_to_csv('input/screenshot-2000.png', 'input/mapping_csv.csv', 'output/2000-mapped-ndvi.png')
    print('Finished 2000')
