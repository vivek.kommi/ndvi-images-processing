import matplotlib
import matplotlib.pyplot as plt
import pandas as pd


def draw_chart(csv_file_path):
    data = pd.read_csv(csv_file_path, sep=';')

    x_axis = []
    old_kazakhstan = []
    new_kazakhstan = []
    photo_modified = []
    photo_modified_moved = []
    output_axis = []

    for x in range(0, len(data.values)):
        x_axis.append(data.values[x][0])
        old_kazakhstan.append(data.values[x][1])
        new_kazakhstan.append(data.values[x][2])
        photo_modified.append(data.values[x][3])
        photo_modified_moved.append(data.values[x][4])
        output_axis.append(data.values[x][5])

    plt.rcParams.update({'font.size': 22})

    fig, ax = plt.subplots()
    ax.plot(x_axis, old_kazakhstan, linewidth=3, label='Kazakhstan 2000', color='green')
    ax.plot(x_axis, new_kazakhstan, linewidth=3, label='Kazakhstan 2020')
    ax.plot(x_axis, photo_modified, linewidth=3, label='2020 sized', linestyle='dashed')
    ax.plot(x_axis, photo_modified_moved, linewidth=3, label='2020 sized and moved', linestyle='dashed', color='purple')
    ax.plot(output_axis, photo_modified, linewidth=6, label='Output', color='red', alpha=0.6)
    ax.set_ylim(ymin=0)
    ax.set_xlim(xmin=0, xmax=255)
    ax.set_xlabel('NDVI intensity [-]', labelpad=20)
    ax.set_ylabel('Occurrences [px]', labelpad=20)

    leg = ax.legend()

    fig.set_size_inches(21, 9)

    plt.tight_layout()
    
    fig.savefig('output/kazakhstan.png', pad_inches=1)


if __name__ == '__main__':
    draw_chart('input/Kazachstan-Fulldata.csv')
