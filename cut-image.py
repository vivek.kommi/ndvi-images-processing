import os
import PIL
import image_slicer
import shutil
from os.path import isfile, join, exists
from os import listdir, makedirs

PIL.Image.MAX_IMAGE_PIXELS = 268435459

img_type = 1

"""
class FileConfig:
    def __init__(self, path, file_type, move_after):
        self.path = path
        self.type = file_type
        self.move_after = move_after


def create_tiles(zoom, file_configs):
    if (zoom >= 7):
        for config in file_configs:
            image_slicer.slice(config, 4)
            files = [f for f in listdir('input/map/') if isfile(join('input/map/', f)) and not f == config.path]
            for file in files:
                if not 'input/map/' + file == 'input/map/map.png':
                    x = int(file[(file.rfind('_') + 1) : file.index('.png')]) - 1
                    y = int(file[4 : file.rfind('_')]) - 1
                    if img_type == 1:
                        x = x + tiles_count
                    elif img_type == 2:
                        y = y + tiles_count
                    elif img_type == 3:
                        x = x + tiles_count
                        y = y + tiles_count
                    output_file_name = 'output/map/' + str(z) + '-' + str(x) + '-' + str(y) + '.png'
                    shutil.move('input/map/' + file, output_file_name)
                    print(output_file_name)
                    img = PIL.Image.open(output_file_name)
                    img = img.resize((256, 256), PIL.Image.ANTIALIAS)
    else:
        for z in range(1, zoom):
            tiles_count = 0
            if z == 1:
                tiles_count = 4
            else:
                tiles_count = 4 ** z
            
            print(tiles_count)
            
            image_slicer.slice('input/map/map.png', tiles_count)
            files = [f for f in listdir('input/map/') if isfile(join('input/map/', f))]
            for file in files:
                if not 'input/map/' + file == 'input/map/map.png':
                    x = int(file[(file.rfind('_') + 1) : file.index('.png')]) - 1
                    y = int(file[4 : file.rfind('_')]) - 1
                    if img_type == 1:
                        x = x + tiles_count
                    elif img_type == 2:
                        y = y + tiles_count
                    elif img_type == 3:
                        x = x + tiles_count
                        y = y + tiles_count
                    output_file_name = 'output/map/' + str(z) + '-' + str(x) + '-' + str(y) + '.png'
                    shutil.move('input/map/' + file, output_file_name)
                    print(output_file_name)
                    img = PIL.Image.open(output_file_name)
                    img = img.resize((256, 256), PIL.Image.ANTIALIAS)
"""

def one_slice(cut_type, path):
    tiles_count = 4

    print(tiles_count)

    image_slicer.slice(path, tiles_count)
    files = [f for f in listdir('input/map/') if isfile(join('input/map/', f))]
    for file in files:
        if not 'input/map/' + file == path:
            x = int(file[(file.rfind('_') + 1) : file.index('.png')]) - 1
            y = int(file[4 : file.rfind('_')]) - 1
            if img_type == 1:
                x = x + tiles_count
            elif img_type == 2:
                y = y + tiles_count
            elif img_type == 3:
                x = x + tiles_count
                y = y + tiles_count
            output_file_name = 'output/map/map-2-' + str(x) + '-' + str(y) + '.png'
            shutil.move('input/map/' + file, output_file_name)
            print(output_file_name)


def slice_to_zoom(path, zoom, addition):
    for z in range(1, zoom):
        tiles_count = 0
        if z == 1:
            tiles_count = 4
        else:
            tiles_count = 4 ** z
        
        print(tiles_count)
        
        image_slicer.slice(path, tiles_count)
        files = [f for f in listdir('input/map/') if isfile(join('input/map/', f))]
        for file in files:
            if not 'input/map/' + file == 'input/map/map.png':
                x = int(file[(file.rfind('_') + 1) : file.index('.png')]) - 1
                y = int(file[4 : file.rfind('_')]) - 1
                if img_type == 1:
                    x = x + tiles_count
                elif img_type == 2:
                    y = y + tiles_count
                elif img_type == 3:
                    x = x + tiles_count
                    y = y + tiles_count
                output_file_name = 'output/map/' + str(z + addition) + '-' + str(x) + '-' + str(y) + '.png'
                shutil.move('input/map/' + file, output_file_name)
                print(output_file_name)
                img = PIL.Image.open(output_file_name)
                img = img.resize((256, 256), PIL.Image.ANTIALIAS)


#if __name__ == "__main__":
    #one_slice(0, 'input/map/map.png')
    # slice_to_zoom(path, zoom, addition)

img_type = 3
for z in range(1, 7):
    tiles_count = 0
    size = 0
    if z == 1:
        tiles_count = 4
        size = 2
    else:
        tiles_count = 4 ** z
        size = 2 ** z
    
    print(tiles_count)
    
    image_slicer.slice('input/map/map.png', tiles_count)
    files = [f for f in listdir('input/map/') if isfile(join('input/map/', f))]
    for file in files:
        if not 'input/map/' + file == 'input/map/map.png':
            x = int(file[(file.rfind('_') + 1) : file.index('.png')]) - 1
            y = int(file[4 : file.rfind('_')]) - 1
            if img_type == 1:
                x = x + size
            elif img_type == 2:
                y = y + size
            elif img_type == 3:
                x = x + size
                y = y + size
            output_file_name = 'output/map-3/' + str(z + 1) + '-' + str(x) + '-' + str(y) + '.png'
            shutil.move('input/map/' + file, output_file_name)
            print(output_file_name)
            
            img = PIL.Image.open(output_file_name)
            img = img.resize((256, 256), PIL.Image.ANTIALIAS)
            img.save(output_file_name)
            img.close()
    